#include "cbs.h"
#include <fstream>
#include <iostream>

#include "json.hpp"

using json = nlohmann::json;



namespace cbs {

    inline void expect(const json& j, const char* entry) {
        if (!j.contains(entry)) throw_error("Expected '" +std::string(entry) +"' entry.");
    }
    inline std::string expect_string(const json& j, const char* entry) {
        expect(j, entry);
        auto& e = j[entry];
        if (!e.is_string()) throw_error("Expected entry '" + std::string(entry) + "' to be a string.");
        return e.get<std::string>();
    }
    inline i32 expect_i32(const json& j, const char* entry) {
        expect(j, entry);
        auto& e = j[entry];
        if (!e.is_number_unsigned()) throw_error("Expected entry '" + std::string(entry) + "' to be an uint.");
        return e.get<i32>();
    }
    inline f32 expect_f32(const json& j, const char* entry) {
        expect(j, entry);
        auto& e = j[entry];
        if (!e.is_number()) throw_error("Expected entry '" + std::string(entry) + "' to be an f32.");
        return e.get<f32>();
    }
    inline vec2i32 expect_ivec2(const json& j, const char* entry) {
        expect(j, entry);
        auto& e = j[entry];
        if (!e.is_array()) throw_error("Expected entry '" + std::string(entry) + "' to be an array.");
        if (e.size() != 2) throw_error("Expected entry '" + std::string(entry) + "' to be an array with two elements.");
        auto &i1 = e[0];
        auto &i2 = e[1];
        if (!i1.is_number_unsigned() || !i2.is_number_unsigned()) throw_error("Expected entry '" + std::string(entry) + "' to contain uints.");
        return {i1.get<i32>(),i2.get<i32>() };
    }

    void ProgramData::parse_input()
    {
        std::cout << "Parsing input" << std::endl;
        std::string name = config_path.stem().string();
        auto filep = config_path;
        output_dir = filep.parent_path();
        if (config_path.extension() != ".json") filep += ".json";
        output_dir /= filep.stem().string();
        std::ifstream i(filep);
        if (!i.is_open()) throw_error("Could not open simulation file: " + config_path.string());
        json j;
        i >> j;

        if (!j.is_object()) throw_error("Expected JSON object.");

        // Load params
        params.k = expect_i32(j, "k");
        params.timeout = expect_f32(j, "timeout");
        params.use_agents = expect_i32(j, "use_agents");

        std::cout << "2^k: " << params.k << ", timeout: " << params.timeout << "s, #agents: " << params.use_agents << std::endl;

        // Load agents
        expect(j, "agents");
        auto& arr = j["agents"];
        if (!arr.is_array()) throw_error("Expected entry 'agents' to be an array.");
        if (params.use_agents > arr.size()) throw_error("use_agents=" + std::to_string(params.use_agents) + " but only " + std::to_string(arr.size()) + " defined.");
        agents.resize(params.use_agents);
        int it = 0;
        for (auto& ag : arr) {
            if (!ag.is_object()) throw_error("Expected 'agents' entry to be an object.");
            auto& a = agents[it];
            a.aid = it;
            a.start_pos = expect_ivec2(ag, "start");
            a.target_pos = expect_ivec2(ag, "target");
            a.start_heading = expect_f32(ag, "heading");
            a.radius = expect_f32(ag, "radius");
            a.movement_speed = expect_f32(ag, "movement_speed");
            a.rotation_speed = expect_f32(ag, "rotation_speed");
            std::cout << "   A" << it << ": [start=" << a.start_pos << ", target=" << a.target_pos << " heading=" << a.start_heading << ", radius=" << a.radius << ", mvt_speed=" << a.movement_speed << ", rot_speed=" << a.rotation_speed << "]" << std::endl;
            ++it;
            if (it >= params.use_agents) break;
        }

        // Load grid
        auto map_dimensions = expect_ivec2(j, "map_dimensions");
        auto map_path = expect_string(j, "map_file");
        grid.set_dimensions(map_dimensions);


        fs::path mapp = map_path;
        if (mapp.extension() != ".txt") mapp += ".txt";
        std::ifstream map_file(mapp);

        std::cout << "map: " << mapp.string() << " " << map_dimensions << std::endl;

        int y = map_dimensions.y - 1;
        for (std::string line; std::getline(map_file, line); )
        {
            int x = 0;
            for (auto c : line) {
                if (c == '#') grid.set_blocked({x,y}, true);
                ++x;
            }
            --y;
        }
    }
    void ProgramData::export_results(Node *res)
    {
        //export_solution(); // JSON of solution & runtime & nodes

        json infos;

        infos["explored_nodes"] = node_count;
        auto& solution = infos["solution"];
        solution["cost"] = res->f_value;
        for (auto& a : agents) {
            auto key_name = "traj_" + std::to_string(a.aid);
            auto& traj = solution[key_name];
            traj = json::array();
            for (auto& i : res->solution[a.aid].actions) {
                traj.push_back(i.to_string(graph));
            }
        }
        auto& c = solution["constraint"];
        c["agent"] = res->constraint.agent;
        c["at"] = res->constraint.state.to_string(graph);
        c["action"] = res->constraint.action.to_string(graph);
        c["interval"] = res->constraint.interval.to_string();


        auto& ancestors = solution["ancestors"];
        ancestors = json::array();

        std::vector<Node*> parents;
        auto parent = res->parent;
        while (parent) {
            parents.push_back(parent);
            parent = parent->parent;
        }

        for (int i = parents.size() - 1; i >= 0; --i) {
            auto p = parents[i];
            json node_info;
            node_info["node_id"] = p->node_id;
            node_info["f_value"] = p->f_value;
            if (p->constraint.agent >= 0) {
                auto& c = node_info["constraint"];
                c["agent"] = p->constraint.agent;
                c["at"] = p->constraint.state.to_string(graph);
                c["action"] = p->constraint.action.to_string(graph);
                c["interval"] = p->constraint.interval.to_string();
            }
            auto& conf = node_info["conflict"];
            auto& a1 = conf["A"];
            a1["agent"] = p->collision.a1.agent;
            a1["at"] = p->collision.a1.state.to_string(graph);
            a1["action"] = p->collision.a1.action.to_string(graph);
            auto& a2 = conf["B"];
            a2["agent"] = p->collision.a2.agent;
            a2["at"] = p->collision.a2.state.to_string(graph);
            a2["action"] = p->collision.a2.action.to_string(graph);
            conf["collision_interval"] = p->collision.collision_interval.to_string();
            conf["overlap_interval"] = p->collision.overlap_interval.to_string();
            ancestors.push_back(node_info);
        }

        auto file_path = output_dir / fs::path("info.json");
        auto file = std::ofstream(file_path, std::ios::out);
        if (!file.is_open()) throw_error("Could not open details file: " + file_path.string());
        file << infos.dump(4);
        file.close();
    }

    constexpr auto COLOR_GRID = "black";
    constexpr auto COLOR_GRAPH = "#bababa";
    constexpr auto COLOR_VERTEX_TEXT = "black";
    constexpr auto COLOR_VERTEX_TEXT_HIGHLIGHT = "orange";
    constexpr auto COLOR_AGENT_MARKER = "grey";
    constexpr int AGENT_COLOR_COUNT = 4;
    const char* AGENT_COLORS[AGENT_COLOR_COUNT] = {
        "orange",
        "red",
        "green",
        "yellow"
    };

    xml::XMLElem* set_style_grid_line(xml::XMLElem* elem) {
        elem->add_attribute("fill", "none");
        elem->add_attribute("stroke", COLOR_GRID);
        elem->add_attribute("stroke-width", "1");
        elem->add_attribute("stroke-linecap", "square");
        return elem;
    }
    xml::XMLElem* set_style_cell(xml::XMLElem* elem) {
        elem->add_attribute("fill", COLOR_GRID);
        return elem;
    }
    xml::XMLElem* set_style_graph_vertex(xml::XMLElem* elem) {
        elem->add_attribute("fill", COLOR_GRAPH);
        return elem;
    }
    xml::XMLElem* set_style_graph_edge(xml::XMLElem* elem) {
        elem->add_attribute("fill", "none");
        elem->add_attribute("stroke", COLOR_GRAPH);
        elem->add_attribute("stroke-width", "0.5");
        return elem;
    }
    xml::XMLElem* set_style_vertex_text(xml::XMLElem* elem, const char*color) {
        elem->add_attribute("fill", color);
        elem->add_attribute("font-size", "10px");
        elem->add_attribute("text-anchor", "middle");
        return elem;
    }
    xml::XMLElem* set_style_grid_text_bottom(xml::XMLElem* elem, const char* color) {
        elem->add_attribute("fill", color);
        elem->add_attribute("font-size", "10px");
        elem->add_attribute("text-anchor", "middle");
        return elem;
    }
    xml::XMLElem* set_style_grid_text_side(xml::XMLElem* elem, const char* color) {
        elem->add_attribute("fill", color);
        elem->add_attribute("font-size", "10px");
        elem->add_attribute("text-anchor", "right");
        return elem;
    }
    xml::XMLElem* set_style_agent_body(xml::XMLElem* elem, int agent_id) {
        elem->add_attribute("fill", AGENT_COLORS[agent_id%AGENT_COLOR_COUNT]);
        return elem;
    }
    xml::XMLElem* set_style_agent_marker(xml::XMLElem* elem) {
        elem->add_attribute("fill", "none");
        elem->add_attribute("stroke", COLOR_AGENT_MARKER);
        elem->add_attribute("stroke-width", "4");
        return elem;
    }
    xml::XMLElem* set_style_agent_traj(xml::XMLElem* elem, int agent_id) {
        elem->add_attribute("fill", "none");
        elem->add_attribute("stroke", AGENT_COLORS[agent_id % AGENT_COLOR_COUNT]);
        elem->add_attribute("stroke-width", "2.0");
        return elem;
    }


    void ProgramData::export_map()
    {
        ViewMatrix v;
        auto doc = gen_map(v, true, true, true);
        xml::export_xml(*doc, output_dir / fs::path("map.svg"), true);
    }

    void ProgramData::export_heuristic()
    {
        int aid = 1;
        for (auto& a : agents) {
            ViewMatrix v;
            auto doc = gen_map(v, true, true, true);

            auto g_costs = xml::XMLElem::new_tag("g");
            g_costs->add_attribute("id", "heuristic_costs");

            // Add cost over each vertex
            int vid = 0;
            for (auto& ve : graph.vertices) {
                auto v_pos = v.pos((vec2f32)ve.pos);
                g_costs->add(set_style_vertex_text(svg::new_text(v_pos + vec2f32(0.0f,15.0f), std::to_string(a.heuristic[vid])), vid == a.target_vertex ? COLOR_VERTEX_TEXT_HIGHLIGHT : COLOR_VERTEX_TEXT));
                ++vid;
            }

            doc->add(g_costs);

            xml::export_xml(*doc, output_dir / fs::path("heuristic_A"+std::to_string(aid)+".svg"), true);
            ++aid;
        }
    }

    void ProgramData::export_solution(const Node& node, std::string name)
    {
        ViewMatrix v;
        auto doc = gen_map(v, true, true, true);

        doc->add(gen_agent_motion(v, node));

        xml::export_xml(*doc, output_dir / fs::path(name+".svg"), true);
    }

    std::unique_ptr<xml::XMLElem> ProgramData::gen_map(ViewMatrix &v, bool gen_grid, bool gen_graph, bool gen_graph_labels)
    {

        constexpr auto MAP_SCALE = 100.f;
        vec2f32 map_size = { MAP_SCALE * (grid.width() + 2), MAP_SCALE * (grid.height() + 2) };

        v = { MAP_SCALE, {MAP_SCALE * 1.5f, map_size.y - MAP_SCALE * 1.5f} };


        auto doc = svg::new_svg();
        svg::set_viewport(*doc, (vec2i32)map_size);

        if (gen_grid) {
            auto g_grid = xml::XMLElem::new_tag("g");
            g_grid->add_attribute("id", "grid");

            // Fill blocked cells
            for (auto y = 0; y < grid.height(); ++y) {
                for (auto x = 0; x < grid.width(); ++x) {
                    if (grid.is_blocked({ x,y })) {
                        g_grid->add(set_style_cell(svg::new_rect(v.pos(vec2f32(x - 0.5f, y - 0.5f)), v.vec({ 1.0f,1.0f }))));
                    }
                }
            }

            // Create grid path
            svg::PathCreator gr;
            for (int i = 0; i <= grid.width(); ++i) {
                gr.move_to(v.pos(vec2f32(i - 0.5f, -0.5f)));
                gr.line_to(v.pos(vec2f32(i - 0.5f, grid.height() - 0.5f)));
            }
            for (int i = 0; i <= grid.height(); ++i) {
                gr.move_to(v.pos(vec2f32(-0.5f, i - 0.5f)));
                gr.line_to(v.pos(vec2f32(grid.width() - 0.5f, i - 0.5f)));
            }
            g_grid->add(set_style_grid_line(gr.get_tag()));

            // Add grid coordinates
            for (int i = 0; i < grid.width(); ++i) {
                g_grid->add(set_style_grid_text_bottom(svg::new_text(v.pos(vec2f32(i, -0.5f))+ vec2f32(0,10), std::to_string(i)), COLOR_GRID));
            }
            for (int i = 0; i < grid.height(); ++i) {
                g_grid->add(set_style_grid_text_side(svg::new_text(v.pos(vec2f32(-0.5f, i)) - vec2f32(10, 0), std::to_string(i)), COLOR_GRID));
            }

            doc->add(g_grid);
        }
        
        if (gen_graph) {

            // Draw graph
            auto g_labels = xml::XMLElem::new_tag("g");
            g_labels->add_attribute("id", "graph_labels");

            auto g_graph = xml::XMLElem::new_tag("g");
            g_graph->add_attribute("id", "graph");
            svg::PathCreator edges;

            // Vertices
            int vid = 0;
            for (auto& ve : graph.vertices) {
                auto v_pos = v.pos((vec2f32)ve.pos);
                g_graph->add(set_style_graph_vertex(svg::new_circle(3.0f, v_pos)));
                if (gen_graph_labels)
                    g_labels->add(set_style_vertex_text(svg::new_text(v_pos - vec2f32(0.0f, 5.0f), std::to_string(vid)), COLOR_VERTEX_TEXT));
                for (auto& e : ve.neighbors) {
                    auto neighbor_pos = v.pos((vec2f32)graph.vertices[e.vertex].pos);
                    auto perp = vec2f32(transitions_vec[e.transition_id].y, -transitions_vec[e.transition_id].x);
                    auto offset = v.vec(perp).normalized() * 1.0f;
                    edges.move_to(v_pos+ offset);
                    edges.line_to(neighbor_pos+ offset);
                }
                ++vid;
            }
            g_graph->add(set_style_graph_edge(edges.get_tag()));

            doc->add(g_graph);
            doc->add(g_labels);

        }

        return doc;
    }

    xml::XMLElem* ProgramData::gen_agent_motion(const ViewMatrix& v, const Node& node)
    {
        constexpr float START_TIME_OFFSET = 1.0;
        constexpr float END_TIME_OFFSET = 1.0;

        auto g_motion = xml::XMLElem::new_tag("g");
        g_motion->add_attribute("id", "agent_motion");

        auto g_paths = xml::XMLElem::new_tag("g");
        g_paths->add_attribute("id", "agent_trajectories");
        g_motion->add(g_paths);

        for (auto& a : agents) {
            svg::PathCreator edges;
            auto pos = a.start_vertex;
            edges.move_to(v.pos((vec2f32)graph.vertices[pos].pos));
            for (auto& ac : node.solution[a.aid].actions) {
                if (ac.type == ActionType::MOVE) {
                    // Add edge
                    pos = ac.target_vertex;
                    edges.line_to(v.pos((vec2f32)graph.vertices[pos].pos));
                }
            }
            g_paths->add(set_style_agent_traj(edges.get_tag(), a.aid));
        }


        float max_dur = 0.0f;
        for (auto& t : node.solution) if (t.cost > max_dur) max_dur = t.cost;

        // TODO stop at collision if any

        float animation_duration = max_dur + START_TIME_OFFSET + END_TIME_OFFSET; // one second before start & one after
        std::string duration_str = std::to_string(animation_duration) + "s";

        for (auto& a : agents) {
            auto& traj = node.solution[a.aid];
            auto g_agent_pos = xml::XMLElem::new_tag("g");
            g_agent_pos->add_attribute("id", "agent"+std::to_string(a.aid+1)+"_pos");
            g_motion->add(g_agent_pos);

            auto g_agent_rotation = xml::XMLElem::new_tag("g");
            g_agent_rotation->add_attribute("id", "agent" + std::to_string(a.aid + 1) + "_rot");
            g_agent_pos->add(g_agent_rotation);

            // Draw agent
            g_agent_rotation->add(set_style_agent_body(svg::new_circle(v.length(a.radius), vec2f32{0,0}), a.aid));
            svg::PathCreator marker;
            marker.move_to({ 0.0f,0.0f });
            marker.line_to(v.vec({ a.radius, 0.0f }));
            g_agent_rotation->add(set_style_agent_marker(marker.get_tag()));

            // Add agent label

            g_agent_pos->add(set_style_vertex_text(svg::new_text({0.f, 10.f}, "A"+std::to_string(a.aid+1)), COLOR_VERTEX_TEXT));

            // Add rotation animation
            std::string rot_values_str;
            std::string rot_keytimes_str;

            rot_keytimes_str += std::to_string(0.0);
            rot_values_str += std::to_string(-a.start_heading);

            float last_time = 0.0;
            float last_rot = a.start_heading;

            g_agent_rotation->add_attribute("transform", "rotate(" + std::to_string(-last_rot) + ")");

            for (int i = 0; i < traj.actions.size() - 1; ++i) {
                auto& a = traj.actions[i];
                if (a.type != ActionType::ROTATE) continue;
                auto actual_time = a.start_time + START_TIME_OFFSET;
                if (actual_time != last_time) {
                    rot_keytimes_str += ";"+std::to_string(actual_time /animation_duration);
                    rot_values_str += ";"+std::to_string(-last_rot);
                }
                auto end_time = traj.actions[i + 1].start_time + START_TIME_OFFSET;
                auto new_rot = transitions_heading[a.transition_id];
                rot_keytimes_str += ";" + std::to_string(end_time / animation_duration);
                rot_values_str += ";" + std::to_string(-new_rot);
                last_rot = new_rot;
                last_time = end_time;
            }

            rot_keytimes_str += ";" + std::to_string(1.0);
            rot_values_str += ";" + std::to_string(-last_rot);

            auto rot_anim = xml::XMLElem::new_tag("animateTransform");
            rot_anim->add_attribute("attributeName", "transform");
            rot_anim->add_attribute("attributeType", "XML");
            rot_anim->add_attribute("type", "rotate");
            rot_anim->add_attribute("dur", duration_str);
            rot_anim->add_attribute("repeatCount", "indefinite");
            rot_anim->add_attribute("values", rot_values_str);
            rot_anim->add_attribute("keyTimes", rot_keytimes_str);
            g_agent_rotation->add(rot_anim);

            // Add translation animation
            std::string trans_values_str;
            std::string trans_keytimes_str;


            last_time = 0.0;
            vec2f32 last_pos = v.pos((vec2f32)a.start_pos);

            g_agent_pos->add_attribute("transform", "translate(" + std::to_string(last_pos.x) + " " + std::to_string(last_pos.y) + ")");

            trans_keytimes_str += std::to_string(last_time);
            trans_values_str += std::to_string(last_pos.x) + " "+ std::to_string(last_pos.y);


            for (int i = 0; i < traj.actions.size() - 1; ++i) {
                auto& a = traj.actions[i];
                if (a.type != ActionType::MOVE) continue;
                auto actual_time = a.start_time + START_TIME_OFFSET;
                if (actual_time != last_time) {
                    trans_keytimes_str += ";" + std::to_string(actual_time / animation_duration);
                    trans_values_str += ";" + std::to_string(last_pos.x) + " " + std::to_string(last_pos.y);
                }
                auto end_time = traj.actions[i + 1].start_time + START_TIME_OFFSET;
                auto new_pos = v.pos((vec2f32)graph.vertices[a.target_vertex].pos);
                trans_keytimes_str += ";" + std::to_string(end_time / animation_duration);
                trans_values_str += ";" + std::to_string(new_pos.x) + " " + std::to_string(new_pos.y);
                last_pos = new_pos;
                last_time = end_time;
            }

            trans_keytimes_str += ";" + std::to_string(1.0);
            trans_values_str += ";" + std::to_string(last_pos.x) + " " + std::to_string(last_pos.y);


            auto trans_anim = xml::XMLElem::new_tag("animateTransform");
            trans_anim->add_attribute("attributeName", "transform");
            trans_anim->add_attribute("attributeType", "XML");
            trans_anim->add_attribute("type", "translate");
            trans_anim->add_attribute("dur", duration_str);
            trans_anim->add_attribute("repeatCount", "indefinite");
            trans_anim->add_attribute("values", trans_values_str);
            trans_anim->add_attribute("keyTimes", trans_keytimes_str);
            g_agent_pos->add(trans_anim);

        }
        return g_motion;
    }

}