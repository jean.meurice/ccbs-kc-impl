#include "xml.h"
#include "utils.h"

namespace xml {


    void export_xml(const XMLElem &xml, fs::path &target, bool format) {
        
        auto file = std::ofstream(target, std::ios::out);
        if (!file.is_open()) {
            throw_error("Could not open output file: "+target.string());
        }

        xml.print(file, "", format);
    }

    std::string escape(const std::string &text) {
        std::string res = "";
        for (auto c : text) {
            switch (c) {
                case '"': res += "&quot;"; break;
                case '\'': res += "&apos;"; break;
                case '<': res += "&lt;"; break;
                case '>': res += "&gt;"; break;
                case '&': res += "&amp;"; break;
                default: res += c; break;
            }
        }
        return res;
    }

    void XMLElem::print(std::ofstream &file, const std::string &indent, bool format) const {
        if (m_is_text) {
            file << escape(name_or_text);
        } else {
            bool is_closed = children.size() == 0;
            file << '<' << name_or_text;
            for (auto &a : attributes) {
                file << ' ' << a.name << "=\"" << escape(a.value) << '"';
            }
            if (is_closed) {
                file << "/>";
                return;
            }
            file << '>';
            if (children.size() == 1 && children[0]->is_text()) {
                children[0]->print(file, indent, false);
            }
            else {
                auto new_indent = indent + INDENT;
                for (auto& c : children) {
                    if (format) {
                        file << std::endl << new_indent;
                    }
                    c->print(file, new_indent, format);
                }
                if (format) {
                    file << std::endl << indent;
                }
            }
            file << "</" << name_or_text << '>';
        }
    }

    /*XMLElem* XMLElem::clone()
    {
        if (is_text()) {
            return new_text(name_or_text);
        }
        else {
            auto new_elem = new_tag(name_or_text);
            new_elem->children
        }
        auto new_elem = new XMLElem();
        return nullptr;
    }*/

}

namespace svg {

    std::unique_ptr<xml::XMLElem> new_svg() {
        auto tag = xml::XMLElem::new_tag("svg");

        tag->add_attribute("xmlns", "http://www.w3.org/2000/svg");
        tag->add_attribute("xmlns:xlink", "http://www.w3.org/1999/xlink");

        return std::unique_ptr<xml::XMLElem>(tag);
    }

    void set_viewport(xml::XMLElem &svg_tag, vec2i32 dimensions) {
        auto x = std::to_string(dimensions.x);
        auto y = std::to_string(dimensions.y);
        svg_tag.add_attribute("width", x+"px");
        svg_tag.add_attribute("height", y+"px");
        svg_tag.add_attribute("viewBox", "0 0 "+x+" "+y);
    }

    xml::XMLElem* new_circle(float radius, vec2f32 position)
    {
        auto tag = xml::XMLElem::new_tag("circle");
        tag->add_attribute("r", std::to_string(radius));
        tag->add_attribute("cx", std::to_string(position.x));
        tag->add_attribute("cy", std::to_string(position.y));
        return tag;
    }

    xml::XMLElem* new_text(vec2f32 position, std::string content)
    {
        auto tag = xml::XMLElem::new_tag("text");
        tag->add_attribute("x", std::to_string(position.x));
        tag->add_attribute("y", std::to_string(position.y));
        tag->add(xml::XMLElem::new_text(content));
        return tag;
    }

    xml::XMLElem* new_rect(vec2f32 position, vec2f32 size)
    {
        if (size.x < 0.0f) {
            position.x += size.x;
            size.x = -size.x;
        }
        if (size.y < 0.0f) {
            position.y += size.y;
            size.y = -size.y;
        }
        auto tag = xml::XMLElem::new_tag("rect");
        tag->add_attribute("x", std::to_string(position.x));
        tag->add_attribute("y", std::to_string(position.y));
        tag->add_attribute("width", std::to_string(size.x));
        tag->add_attribute("height", std::to_string(size.y));
        return tag;
    }


    void test_svg_export(fs::path target) {
        auto doc = new_svg();
        set_viewport(*doc, {300,200});

        auto c = new_circle(30, { 50,50 });
        c->add_attribute("fill", "orange");
        doc->add(c);

        xml::export_xml(*doc, target, true);
    }
    xml::XMLElem* PathCreator::get_tag()
    {
        auto tag = xml::XMLElem::new_tag("path");
        std::string command_string = "";
        for (auto& c : commands) {
            switch (c.type) {
            case MOVE:
                command_string += " M " + std::to_string(c.point.x) + " " + std::to_string(c.point.y);
                break;
            case LINE_TO:
                command_string += " L " + std::to_string(c.point.x) + " " + std::to_string(c.point.y);
                break;
            }
        }
        if (closed) command_string += " Z";
        tag->add_attribute("d", command_string);
        return tag;
    }
    void PathCreator::move_to(f32 x, f32 y)
    {
        commands.push_back({ MOVE, {x,y} });
    }
    void PathCreator::line_to(f32 x, f32 y)
    {
        commands.push_back({ LINE_TO, {x,y} });
    }
}