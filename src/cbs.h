#pragma once
#include <vector>
#include <memory>
#include "utils.h"
#include "xml.h"


namespace cbs {
    constexpr usize TRANSITION_COUNT = 16;
    extern const vec2i32 transitions[TRANSITION_COUNT];
    extern vec2f32 transitions_vec[TRANSITION_COUNT];
    extern float transitions_heading[TRANSITION_COUNT];
    extern const usize k_to_transition_count[];
    extern const vec2f32 cell_corners[];

    struct Grid {

        void set_dimensions(vec2i32 dimensions) {
            this->dimensions = dimensions;
            blocked.assign((usize)dimensions.x * dimensions.y, false);
            vertices.assign((usize)dimensions.x * dimensions.y, -1);
        }

        inline bool is_valid(vec2i32 pos) {
            return pos.x >= 0 && pos.y >= 0 && pos.x < dimensions.x && pos.y < dimensions.y;
        }

        inline usize index(vec2i32 pos) {
            if (!is_valid(pos)) throw_error("Grid lookup '(" + std::to_string(pos.x) + ", " + std::to_string(pos.y) + ")' outside of range '(" + std::to_string(dimensions.x) + ", " + std::to_string(dimensions.y) + ")'");
            return (usize)pos.x + (usize)pos.y * (usize)dimensions.x;
        }

        bool is_blocked(vec2i32 pos) {
            return blocked[index(pos)];
        }
        void set_blocked(vec2i32 pos, bool block) {
            blocked[index(pos)] = block;
        }
        i32 get_vertex(vec2i32 pos) {
            return vertices[index(pos)];
        }
        void set_vertex(vec2i32 pos, i32 vertex) {
            vertices[index(pos)] = vertex;
        }

        i32 width() {
            return dimensions.x;
        }
        i32 height() {
            return dimensions.y;
        }

        f32 get_clearance(vec2i32 pos, vec2i32 target_pos);

    private:
        vec2i32 dimensions;
        std::vector<bool> blocked;
        std::vector<i32> vertices;
    };

    struct Edge {
        i32 vertex;
        float max_radius;
        float length;
        i32 transition_id;
        i32 eid; // Index inside the vertex

        Edge(i32 vertex, float max_radius, float length, i32 transition_id, i32 eid) : vertex(vertex), max_radius(max_radius), length(length), transition_id(transition_id), eid(eid) {}
    };

    struct Vertex {
        i32 vid;
        vec2i32 pos;
        std::vector<Edge> neighbors;
        //std::vector<i32> transition_id_to_edge_id;

        Vertex(i32 vid, vec2i32 pos) : vid(vid), pos(pos) {}
    };

    struct Graph {
        i32 vertex_count = 0;
        std::vector<Vertex> vertices;
    };

    enum class ActionType {
        WAIT,
        MOVE,
        ROTATE
    };

    struct Action {
        ActionType type;
        i32 target_vertex;
        i32 transition_id;
        float start_time;

        static Action new_wait(float start_time) {
            return Action(ActionType::WAIT, -1, 0, start_time);
        }
        static Action new_move(float start_time, i32 target_vertex, i32 transition_id) {
            return Action(ActionType::MOVE, target_vertex, transition_id, start_time);
        }
        static Action new_rotate(float start_time, i32 transition_id) {
            return Action(ActionType::ROTATE, -1, transition_id, start_time);
        }
        Action() : type(ActionType::WAIT), start_time(0.0) {}

        std::string to_string(const Graph& g) const;
    private:
        Action(ActionType type, i32 target_vertex, i32 transition_id, float start_time) : type(type), target_vertex(target_vertex), transition_id(transition_id), start_time(start_time) {}
    };

    struct State {
        i32 state_id;
        i32 vertex;
        i32 transition_id;

        State(i32 state_id, i32 vertex, i32 transition_id) : state_id(state_id), vertex(vertex), transition_id(transition_id) {}
        State() : state_id(-1), vertex(-1), transition_id(-1) {}

        std::string to_string(const Graph& g) const;
    };

    struct Trajectory {
        i32 aid;
        std::vector<Action> actions;
        float cost = 0.0;

        Trajectory() : aid(-1), cost(NAN) {}
    };

    constexpr float CONSTRAINT_EPSILON = 0.001f;
    constexpr float RADIUS_EPSILON = 0.01f;
    constexpr float OVERLAP_EPSILON = 0.00001f;
    struct Interval {
        float start = 0.0;
        float end = 0.0;

        bool overlaps(const Interval& other) const {
            return other.start < this->end - OVERLAP_EPSILON && this->start < other.end - OVERLAP_EPSILON;
        }

        static Interval full_interval() {
            return {0.0, std::numeric_limits<float>::infinity()};
        }

        std::string to_string() const {
            return "[" + std::to_string(start) + "; " + std::to_string(end) + "]";
        }
    };

    struct Constraint {
        i32 agent = -1;
        State state;
        Action action;
        Interval interval;
        float end_time;

        void init(i32 agent, const State &state, const Action &action, float end_time) {
            this->agent = agent;
            this->state = state;
            this->action = action;
            this->end_time = end_time;
        }
    };

    struct Collision {
        Constraint a1;
        Constraint a2;
        Interval overlap_interval; // Time shared between the two actions
        Interval collision_interval; // Time at which the collision actually occurs

        void print(const Graph& g);
    };


    using Solution = std::vector<Trajectory>;

    struct Node {
        i32 node_id;
        Node* parent = nullptr;
        std::vector<std::unique_ptr<Node>> children;
        Collision collision;
        Constraint constraint;
        Solution solution;
        float f_value = 0.0;

        void compute_f_value();

        Node(i32 node_id) : node_id(node_id) {}
    };

    struct Params {
        i32 k = 0;
        float timeout = 0.0;
        i32 use_agents = 0;
        i32 transition_count = 0;
        bool export_node_solutions = false;
        bool no_timeout = false;
        bool export_intermediary_solutions = false;
    };

    struct Agent {
        i32 aid;
        vec2i32 start_pos;
        float start_heading;
        i32 start_transition_id;
        i32 start_vertex = -1;
        i32 target_vertex = -1;
        vec2i32 target_pos;
        float radius;
        float movement_speed;
        float rotation_speed; // Degrees/sec
        std::vector<float> heuristic; // Distance to target_pos for every vertex
    };


    struct ViewMatrix {
        float scale = 0.0; // 1 unit = how many pixels
        vec2f32 offset = {0.0,0.0}; // Where (0,0) is on the screen


        vec2f32 pos(vec2f32 coord) const {
            coord.y = -coord.y;
            return (coord * scale) + offset;
        }
        vec2f32 vec(vec2f32 v) const {
            v.y = -v.y;
            return v * scale;
        }
        f32 length(f32 l) const {
            return l * scale;
        }
    };



    struct ProgramData {
        fs::path config_path;
        fs::path output_dir;
        Params params;
        Grid grid;
        Graph graph;
        std::vector<Agent> agents;
        std::unique_ptr<Node> tree;
        i32 node_count = 0;
        Timer timer;

        ProgramData(fs::path config_path) : config_path(config_path) {}

        void entry_point();
        void parse_input();
        void load();
        Node *search();
        bool simulate(Collision& collision, std::vector<Trajectory> &solution, i32 agent, bool simulate_all);
        bool pathfind(Node *node, i32 agent);
        void update_intervals(std::vector<Interval> &intervals, const Constraint &constraint);
        bool check_collisions(Collision &collision);
        void create_children(Node *node);
        void resolve_collision(Constraint& target, const Constraint &other, const Collision &collision);
        void export_results(Node* res);
        void export_map();
        void export_heuristic();
        void export_solution(const Node& node, std::string name);
        std::unique_ptr<xml::XMLElem> gen_map(ViewMatrix& v, bool gen_grid, bool gen_graph, bool gen_graph_lables); // Generates the svg doc and returns it, also initiates the "view-matrix"
        xml::XMLElem* gen_agent_motion(const ViewMatrix& v, const Node& node);
    };


}
