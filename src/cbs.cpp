#include "cbs.h"
#include <queue>
#include <iostream>
#include <cmath>

namespace cbs {
    // For k=2,3,4
    const vec2i32 transitions[TRANSITION_COUNT] = {
        {0,1},
        {1,0},
        {0,-1},
        {-1,0},
        {1,1},
        {-1,1},
        {1,-1},
        {-1,-1},
        {1,2},
        {-1,2},
        {1,-2},
        {-1,-2},
        {2,1},
        {-2,1},
        {2,-1},
        {-2,-1},
    };
    vec2f32 transitions_vec[TRANSITION_COUNT];
    float transitions_heading[TRANSITION_COUNT];
    const usize k_to_transition_count[]{
        0,0,4,8,16
    };
    const vec2f32 cell_corners[]{
        {0.5f,0.5f},
        {0.5f,-0.5f},
        {-0.5f,0.5f},
        {-0.5f,-0.5f},
    };


    inline float angle_between(float a, float b) {
        if (b > a) std::swap(a, b);
        a -= b;
        return std::min(a, 360.0f - a);
    }

    void ProgramData::entry_point()
    {
        parse_input();
        load();
        auto res = search();
        if (!res) {
            std::cout << "Could not find solution." << std::endl;
            return;
        }
        else {
            std::cout << "Found solution!" << std::endl;
            export_solution(*res, "final_solution");
            if (params.export_intermediary_solutions && !params.export_node_solutions) {
                // Also export the parents of the final solution
                auto parent = res->parent;
                while (parent) {
                    export_solution(*parent, "solution_" + std::to_string(parent->node_id));
                    parent = parent->parent;
                }
            }
        }
        
        export_results(res);
    }
    void ProgramData::load()
    {
        fs::create_directories(output_dir);

        std::cout << "Loading data-structures." << std::endl;
        params.transition_count = (i32)k_to_transition_count[params.k];
        for (auto i = 0; i < TRANSITION_COUNT; ++i) {
            transitions_vec[i] = ((vec2f32)transitions[i]).normalized();
            transitions_heading[i] = std::atan2(transitions_vec[i].y, transitions_vec[i].x) * RAD_TO_DEGf;
        }

        f32 min_radius = std::numeric_limits<f32>::infinity();
        for (auto& a : agents) {
            if (a.radius < min_radius) min_radius = a.radius;
            while (a.start_heading > 180.0f) a.start_heading -= 360.0f;
            while (a.start_heading < -180.0f) a.start_heading += 360.0f;
            float closest = std::numeric_limits<float>::infinity();
            for (auto i = 0; i < params.transition_count; ++i) {
                auto angle = angle_between(a.start_heading, transitions_heading[i]);
                if (angle < closest) {
                    closest = angle;
                    a.start_transition_id = i;
                }
            }
            a.start_heading = transitions_heading[a.start_transition_id];
        }


        // Create graph

        // Create non-blocked vertices
        graph.vertex_count = 0;
        for (int y = 0; y < grid.height(); ++y) {
            for (int x = 0; x < grid.width(); ++x) {
                auto pos = vec2i32(x, y);
                if (!grid.is_blocked(pos)) {
                    grid.set_vertex(pos, graph.vertex_count);
                    graph.vertices.emplace_back(graph.vertex_count, pos);
                    ++graph.vertex_count;
                }
            }
        }

        // Create edges
        for (auto &v : graph.vertices) {
            int eid = 0;
            //v.transition_id_to_edge_id.assign(params.transition_count, -1);
            for (i32 t = 0; t < params.transition_count; ++t) {
                auto target_pos = v.pos + transitions[t];
                if (!grid.is_valid(target_pos)) continue;
                if (grid.is_blocked(target_pos)) continue;
                auto cl = grid.get_clearance(v.pos, target_pos);
                if (cl < min_radius) continue;
                //v.transition_id_to_edge_id[t] = v.neighbors.size();
                v.neighbors.emplace_back(grid.get_vertex(target_pos), cl, ((vec2f32)(target_pos - v.pos)).length(), t, eid);
                ++eid;
            }
        }


        // Compute heuristics
        // Dijkstra from every target location
        struct NodeRef {
            i32 vertex;
            float cost;
            NodeRef(i32 vertex, float cost) : vertex(vertex), cost(cost) {}
        };
        struct NodeRefComp {
            bool operator()(const NodeRef& a, const NodeRef& b) {
                return a.cost > b.cost;
            }
        };

        std::priority_queue<NodeRef, std::vector<NodeRef>, NodeRefComp> queue;
        std::vector<bool> is_closed;

        for (auto& a : agents) {
            a.start_vertex = grid.get_vertex(a.start_pos);
            a.target_vertex = grid.get_vertex(a.target_pos);
            auto& costs = a.heuristic;

            costs.assign(graph.vertex_count, std::numeric_limits<f32>::infinity());
            is_closed.assign(graph.vertex_count, false);

            costs[a.target_vertex] = 0.0;
            queue.emplace(a.target_vertex, 0.0);

            while (!queue.empty()) {
                auto next = queue.top();
                queue.pop();
                if (is_closed[next.vertex]) continue;
                is_closed[next.vertex] = true;

                for (auto &e : graph.vertices[next.vertex].neighbors) {
                    if (is_closed[e.vertex]) continue;
                    if (a.radius > e.max_radius) continue;
                    auto target_costs = next.cost + e.length / a.movement_speed;
                    if (target_costs < costs[e.vertex]) {
                        costs[e.vertex] = target_costs;
                        queue.emplace(e.vertex, target_costs);
                    }
                }
            }
        }

        export_map(); // SVG of the map
        export_heuristic();
    }



    Node* ProgramData::search()
    {
        std::cout << "Initiating the search tree." << std::endl;
        // Init the tree
        tree = std::make_unique<Node>(node_count);
        ++node_count;
        tree->solution.resize(agents.size());
        for (int i = 0; i < agents.size(); ++i) {
            if (!pathfind(tree.get(), i)) {
                std::cout << "Could not find initial solution for agent " << i << std::endl;
                return nullptr;
            }
        }
        if (!simulate(tree->collision, tree->solution, -1, true)) return tree.get();
        if (params.export_node_solutions)
            tree->collision.print(graph);
        export_solution(*tree, "solution_" + std::to_string(tree->node_id));
        create_children(tree.get());






        std::cout << "Performing the search." << std::endl;

        // Search the tree
        struct NodeRef {
            Node *node;
            float cost;
            NodeRef(Node* node, float cost) : node(node), cost(cost) {}
        };
        struct NodeRefComp {
            bool operator()(const NodeRef& a, const NodeRef& b) {
                return a.cost > b.cost;
            }
        };

        std::priority_queue<NodeRef, std::vector<NodeRef>, NodeRefComp> queue;
        for (auto& c : tree->children) queue.emplace(c.get(), c->f_value);

        timer.start();
        while (!queue.empty()) {
            auto node_ref = queue.top();
            queue.pop();
            auto node = node_ref.node;
            if (!simulate(node->collision, node->solution, node->constraint.agent, false)) return node;
            if (params.export_node_solutions)
                node->collision.print(graph);
            create_children(node);
            for (auto& child : node->children) {
                queue.emplace(child.get(), child->f_value);
            }
            timer.end();
            auto time = timer.duration();
            if (time > params.timeout && !params.no_timeout) {
                std::cout << "Timeout!" << std::endl;
                return nullptr;
            }
        }
        return nullptr;
    }
    bool ProgramData::simulate(Collision& collision, std::vector<Trajectory>& solution, i32 agent, bool simulate_all)
    {
        simulate_all = true;
        const auto num_agents = solution.size();
        std::vector<Action> active_action(num_agents, Action::new_wait(0.0));
        std::vector<State> agent_state(num_agents); // At start of current action
        std::vector<float> active_action_end_time(num_agents, std::numeric_limits<f32>::infinity());
        std::vector<i32> active_action_id(num_agents, 0);

        struct ActionRef {
            i32 agent;
            float time;
            ActionRef(i32 agent, float time) : agent(agent), time(time) {}
        };
        struct ActionRefComp {
            bool operator()(const ActionRef& a, const ActionRef& b) {
                return a.time > b.time;
            }
        };

        std::priority_queue<ActionRef, std::vector<ActionRef>, ActionRefComp> queue;

        // Initialize
        for (int i = 0; i < num_agents; ++i) {
            auto& state = agent_state[i];
            state.vertex = agents[i].start_vertex;
            state.transition_id = agents[i].start_transition_id;
            auto& traj = solution[i];
            auto number_of_actions = traj.actions.size();
            if (number_of_actions == 0) throw_error("Expected at least one action in a trajectory (wait)");
            active_action[i] = traj.actions[0];
            if (number_of_actions >= 2) {
                auto next_time = traj.actions[1].start_time;
                active_action_end_time[i] = next_time;
                queue.emplace(i, next_time);
            }
        }

        // Check collisions for first actions
        for (int i = 0; i < num_agents; ++i) {
            if (simulate_all || i == agent) {
                for (int j = 0; j < num_agents; ++j) {
                    if (j == i) continue;
                    collision.a1.init(i, agent_state[i], active_action[i], active_action_end_time[i]);
                    collision.a2.init(j, agent_state[j], active_action[j], active_action_end_time[j]);
                    if (check_collisions(collision)) return true;
                }
            }
        }

        // Discrete Time iteration
        while (!queue.empty()) {
            auto ref = queue.top();
            queue.pop();
            // Apply current action on state
            auto& active_a = active_action[ref.agent];
            auto& state = agent_state[ref.agent];
            auto& sol = solution[ref.agent];
            if (active_a.type == ActionType::MOVE) {
                state.vertex = active_a.target_vertex;
            }
            else if (active_a.type == ActionType::ROTATE) {
                state.transition_id = active_a.transition_id;
            }
            // Update lookup grid (TODO implement later)
                // Remove
                // Add
            // Update current action & id
            auto& action_id = active_action_id[ref.agent];
            ++action_id;
            active_a = sol.actions[action_id];
            // Add next action to queue
            if ((usize)action_id + 1 < sol.actions.size()) {
                auto next_time = sol.actions[(usize)action_id + 1].start_time;
                queue.emplace(ref.agent, next_time);
                active_action_end_time[ref.agent] = next_time;
            }
            else {
                active_action_end_time[ref.agent] = std::numeric_limits<float>::infinity();
            }

            // Check if this agent must be checked
            // Loop over all other agents
            // Check collision
            if (simulate_all || ref.agent == agent) {
                for (int j = 0; j < num_agents; ++j) {
                    if (j == ref.agent) continue;
                    collision.a1.init(ref.agent, agent_state[ref.agent], active_action[ref.agent], active_action_end_time[ref.agent]);
                    collision.a2.init(j, agent_state[j], active_action[j], active_action_end_time[j]);
                    if (check_collisions(collision)) return true;
                }
            }
            else {
                collision.a1.init(ref.agent, agent_state[ref.agent], active_action[ref.agent], active_action_end_time[ref.agent]);
                collision.a2.init(agent, agent_state[agent], active_action[agent], active_action_end_time[agent]);
                if (check_collisions(collision)) return true;
            }
        }

        return false;
    }

    bool ProgramData::pathfind(Node* node, i32 agent)
    {
        // TODO put as fixed part of graph => Create "Vertex" struct with position, neighbors, ...
        struct VertexInfo {
            std::vector<Interval> vertex_safe_intervals;
            std::vector<i32> state_id;
            std::vector<std::vector<Interval>> edge_safe_intervals;
        };

        // Init safe intervals
        std::vector<VertexInfo> vertex_info(graph.vertex_count);
        for (auto &v : graph.vertices) {
            auto& vi = vertex_info[v.vid];
            vi.vertex_safe_intervals.resize(1);
            vi.vertex_safe_intervals[0] = Interval::full_interval();
            auto& edges = v.neighbors;
            vi.edge_safe_intervals.resize(edges.size());
            for (auto &esi : vi.edge_safe_intervals) {
                esi.resize(1);
                esi[0] = Interval::full_interval();
            }
        }
        auto& a = agents[agent];
        auto& sv = vertex_info[a.start_vertex];
        if (sv.vertex_safe_intervals[0].start > 0.0f) return false; // The agent is at a constrained position at the start

        // Create safe intervals (for vertices & edges) => traverse constraints
        auto* next = node;

        do {
            auto& constraint = next->constraint;
            if (constraint.agent == agent) {
                // Add constraint to safe intervals
                if (constraint.action.type == ActionType::MOVE) {
                    for (auto& e : graph.vertices[constraint.state.vertex].neighbors) {
                        if (e.vertex == constraint.action.target_vertex) {
                            update_intervals(vertex_info[constraint.state.vertex].edge_safe_intervals[e.eid], constraint);
                            break;
                        }
                    }
                }
                else {
                    update_intervals(vertex_info[constraint.state.vertex].vertex_safe_intervals, constraint);
                }
            }
            next = next->parent;
        } while (next);


        struct SIPPState {
            i32 vertex;
            i32 interval_id;
            std::vector<float> heading_cost; // Use transition_id => heading mapping
            std::vector<i32> heading_predecessor; // State id

            SIPPState(i32 vertex, i32 interval_id, i32 transition_count) : 
                vertex(vertex), interval_id(interval_id), 
                heading_cost(transition_count, std::numeric_limits<float>::infinity()),
                heading_predecessor(transition_count, -1)
            {}
            SIPPState() : vertex(-1), interval_id(-1) {}
        };

        i32 state_count = 0;
        for (auto& vi : vertex_info) state_count += (i32)vi.vertex_safe_intervals.size();
        std::vector<SIPPState> states(state_count);
        int vertex_id = 0;
        int state_id = 0;
        for (auto& si : vertex_info) {
            si.state_id.resize(si.vertex_safe_intervals.size());
            for (int i = 0; i < si.vertex_safe_intervals.size(); ++i) {
                si.state_id[i] = state_id;
                states[state_id] = { vertex_id, i, params.transition_count };
                ++state_id;
            }
            ++vertex_id;
        }


        // https://en.wikipedia.org/wiki/A*_search_algorithm


        struct StateRef {
            i32 state;
            i32 prev_state;
            float cost;
            float f_value;
            i32 transition_id;
            StateRef(i32 state, i32 prev_state, float cost, float f_value, i32 transition_id) : state(state), prev_state(prev_state), cost(cost), f_value(f_value), transition_id(transition_id) {}
        };
        struct StateRefComp {
            bool operator()(const StateRef& a, const StateRef& b) {
                return a.f_value > b.f_value;
            }
        };


        std::priority_queue<StateRef, std::vector<StateRef>, StateRefComp> queue;
        //std::vector<bool> is_closed(state_count, false);

        queue.emplace(sv.state_id[0], -1, 0.0, a.heuristic[a.start_vertex], a.start_transition_id);        

        while (!queue.empty()) {
            auto ref = queue.top();
            auto& state = states[ref.state];
            if (state.vertex == a.target_vertex) {
                // Backtrace
                std::vector<Action> actions;
                actions.push_back(Action::new_wait(ref.cost));

                auto& traj = node->solution[agent];
                traj.cost = ref.cost;

                float current_time = ref.cost;
                auto current_state = ref.state;
                auto current_heading = ref.transition_id;
                auto current_pred = ref.prev_state;
                do {
                    auto& cs = states[current_state];
                    if (current_pred < 0) {
                        if (a.start_transition_id != current_heading) {
                            actions.push_back(Action::new_rotate(0.0, current_heading));
                        }
                        break;
                    }
                    auto &pred_state = states[current_pred];
                    auto& pred_vertex = graph.vertices[pred_state.vertex];
                    auto pred_transition_id = -1;
                    int eid = 0;
                    for (auto& e : pred_vertex.neighbors) {
                        if (e.vertex == cs.vertex) {
                            pred_transition_id = e.transition_id;
                            break;
                        }
                        ++eid;
                    }
                    //auto eid = pred_vertex.transition_id_to_edge_id[pred_transition_id];
                    if (pred_transition_id < 0) {
                        throw_error("Could not find predecessor edge");
                    }
                    auto travel_time = pred_vertex.neighbors[eid].length / a.movement_speed;
                    if (current_heading != pred_transition_id) {
                        // Add rotation action
                        current_time = cs.heading_cost[pred_transition_id];
                        actions.push_back(Action::new_rotate(current_time, current_heading));
                    }

                    // Add move action to reach the current state on the arrival time
                    auto departure_time = current_time - travel_time;
                    actions.push_back(Action::new_move(departure_time, cs.vertex, pred_transition_id));

                    current_time = pred_state.heading_cost[pred_transition_id];
                    if (departure_time > current_time + 0.000001f) {
                        // Add wait action before making the move
                        actions.push_back(Action::new_wait(current_time));
                    }
                    current_state = current_pred;
                    current_heading = pred_transition_id;
                    current_pred = pred_state.heading_predecessor[pred_transition_id];

                } while (true);

                auto num_actions = actions.size();
                traj.actions.resize(num_actions);
                for (int i = 0; i < num_actions; ++i) {
                    traj.actions[i] = actions[num_actions - 1 - i];
                }

                return true;
            }
            queue.pop();

            if (state.heading_cost[ref.transition_id] < ref.cost) continue; // Just check the costs?
            state.heading_cost[ref.transition_id] = ref.cost;
            state.heading_predecessor[ref.transition_id] = ref.prev_state;

            auto& vi = vertex_info[state.vertex];

            // Safe Interval to stay at the vertex
            auto safe_interval = vi.vertex_safe_intervals[state.interval_id];
            safe_interval.start = ref.cost;

            
            for (const auto &edge : graph.vertices[state.vertex].neighbors) {
                if (a.radius > edge.max_radius) continue;
                // Valid departure time considering the rotation time to point to the neighbor edge
                auto departure_interval = safe_interval;
                departure_interval.start += angle_between(transitions_heading[ref.transition_id], transitions_heading[edge.transition_id]) / a.rotation_speed;
                if (departure_interval.start + 0.00001f > departure_interval.end) continue;
                if (state.heading_cost[edge.transition_id] < departure_interval.start) continue; // Prune when the edge can already be reached from another predecessor
                state.heading_cost[edge.transition_id] = departure_interval.start;
                state.heading_predecessor[edge.transition_id] = ref.prev_state;

                auto travel_duration = edge.length / a.movement_speed;
                // Valid arrival interval considering the time to travel
                Interval possible_arrival_interval = { departure_interval.start+ travel_duration, departure_interval.end+ travel_duration };

                
                const auto& neighbor_vi = vertex_info[edge.vertex];
                for (int si_id = 0; si_id < neighbor_vi.vertex_safe_intervals.size(); ++si_id) {
                    auto& si = neighbor_vi.vertex_safe_intervals[si_id];
                    // Find a potential neighbor state (in the time domain)
                    if (!si.overlaps(possible_arrival_interval)) continue;
                    // Possible arrival time: overlap between the two intervals (possible arrival time and the state's safe interval)
                    Interval arrival_interval = { std::max(si.start, possible_arrival_interval.start), std::min(si.end, possible_arrival_interval.end) };

                    // Check the edge for a valid traversal time
                    bool has_any = false;
                    Interval possible_departure_time = { arrival_interval.start -travel_duration, arrival_interval.end - travel_duration };
                    float arrival_time = 0.0;
                    for (auto& edge_si : vi.edge_safe_intervals[edge.eid]) {
                        if (edge_si.overlaps(possible_departure_time)) {
                            has_any = true;
                            arrival_time = std::max(possible_departure_time.start, edge_si.start) + travel_duration;
                            break;
                        }
                    }
                    if (!has_any) continue;
                    auto neighbor_state_id = neighbor_vi.state_id[si_id];
                    auto& neighbor_state = states[neighbor_state_id];
                    if (neighbor_state.heading_cost[edge.transition_id] < arrival_time) continue; // Check cost vs arrival_time
                    queue.emplace(neighbor_state_id, ref.state, arrival_time, arrival_time + a.heuristic[edge.vertex], edge.transition_id);
                }
            }
        }
        return false;
    }

    void ProgramData::update_intervals(std::vector<Interval>& intervals, const Constraint& constraint)
    {

        for (auto i = 0; i < intervals.size(); ++i) {
            auto& si = intervals[i];
            if (!si.overlaps(constraint.interval)) continue;
            if (constraint.interval.start > si.start) {
                if (constraint.interval.end < si.end) {
                    // Constraint is contained in the safe-interval => split it
                    auto old_end = si.end;
                    si.end = constraint.interval.start;
                    ++i;
                    intervals.insert(intervals.begin()+i, Interval{ constraint.interval.end, old_end });
                }
                else {
                    // Shorten the interval
                    si.end = constraint.interval.start;
                }
            }
            else {
                if (constraint.interval.end < si.end) {
                    // Shorten the interval
                    si.start = constraint.interval.end;
                }
                else {
                    // Interval is contained in constraint, Remove the interval
                    intervals.erase(intervals.begin() + i);
                    --i;
                }
            }
        }
    }

    bool ProgramData::check_collisions(Collision& collision)
    {

        collision.overlap_interval.start = std::max(collision.a1.action.start_time, collision.a2.action.start_time);
        collision.overlap_interval.end = std::min(collision.a1.end_time, collision.a2.end_time);
        auto& agent1 = agents[collision.a1.agent];
        auto& agent2 = agents[collision.a2.agent];

        auto d = agent1.radius + agent2.radius;
        auto S1 = graph.vertices[collision.a1.state.vertex].pos;
        auto S2 = graph.vertices[collision.a2.state.vertex].pos;

        if (collision.a1.action.type != ActionType::MOVE && collision.a2.action.type != ActionType::MOVE) {
            auto delta = (vec2f32)(S1 - S2);
            return dot(delta, delta) < d * d;
        }

        auto dt = collision.a2.action.start_time - collision.a1.action.start_time;

        vec2f32 v1 = { 0.0f,0.0f };
        if (collision.a1.action.type == ActionType::MOVE) {
            v1 = transitions_vec[collision.a1.action.transition_id] * agent1.movement_speed;
        }
        vec2f32 v2 = { 0.0f,0.0f };
        if (collision.a2.action.type == ActionType::MOVE) {
            v2 = transitions_vec[collision.a2.action.transition_id] * agent2.movement_speed;
        }

        auto Q = (vec2f32)(S1 - S2) + v2 * dt;
        auto dv = v1 - v2;

        float a1 = dot(dv, dv);
        if (a1 < 0.000001f) return false; // Unstable case
        float b1 = 2.0f * dot(Q,dv);
        float c1 = dot(Q, Q) - d * d;

        float rho1 = b1 * b1 - 4.0f * a1 * c1;
        if (rho1 <= 0.001f) return false; // No solution to the equation

        auto sqrt_rho = std::sqrt(rho1);
        auto t1 = (-b1 + sqrt_rho) / (2.0f * a1);
        auto t2 = (-b1 - sqrt_rho) / (2.0f * a1);
        if (t1 > t2) {
            std::swap(t1, t2);
        }

        collision.collision_interval = {t1 + collision.a1.action.start_time , t2 + collision.a1.action.start_time };

        return collision.overlap_interval.overlaps(collision.collision_interval);
    }

    void ProgramData::create_children(Node* node)
    {
        const auto num_agents = agents.size();
        for (int i = 0; i < 2; ++i) {
            auto child = new Node(node_count);
            ++node_count;

            auto constrained_agent = i ? node->collision.a1.agent : node->collision.a2.agent;

            child->constraint = constrained_agent == node->collision.a1.agent ? node->collision.a1 : node->collision.a2;
            child->parent = node;

            resolve_collision(child->constraint, constrained_agent == node->collision.a1.agent ? node->collision.a2 : node->collision.a1, node->collision);

            child->solution.resize(num_agents);
            bool no_solution = false;
            for (auto t = 0; t < num_agents; ++t) {
                if (t == constrained_agent) {
                    if (!pathfind(child, child->constraint.agent)) {
                        // remove the node
                        no_solution = true;
                        break;
                    }
                }
                else {
                    child->solution[t] = node->solution[t];
                }
            }
            if (no_solution) {
                delete child;
                --node_count;
                continue;
            }
            child->compute_f_value();
            node->children.emplace_back(child);

            if (params.export_node_solutions)
                export_solution(*child, "solution_" + std::to_string(child->node_id));
        }
    }

    void ProgramData::resolve_collision(Constraint& target, const Constraint& other, const Collision& coll)
    {
        if (target.action.type != ActionType::MOVE && other.action.type != ActionType::MOVE) {
            target.interval.start = coll.overlap_interval.start - CONSTRAINT_EPSILON;
            target.interval.end = coll.overlap_interval.end+CONSTRAINT_EPSILON;
            return;
        }

        target.interval.start = target.action.start_time - CONSTRAINT_EPSILON;

        if (target.action.type == ActionType::MOVE && other.action.type != ActionType::MOVE) {
            auto delay = other.end_time - coll.collision_interval.start;
            if (delay < -0.00001f) throw_error("Delay negative");
            auto interval_end = target.action.start_time + delay;
            target.interval.end = std::min(interval_end, coll.overlap_interval.end) + CONSTRAINT_EPSILON; // Constrained intervals can only be between the 2 actions
            return;
        }

        if (target.action.type != ActionType::MOVE && other.action.type == ActionType::MOVE) {
            target.interval.end = std::min(coll.collision_interval.end, coll.overlap_interval.end) + CONSTRAINT_EPSILON;
            return;
        }

        // Both move

        // agent 1: free
        // agent 2: constrained (search for the length of its wait action)

        const auto& target_agent = agents[target.agent];
        const auto& other_agent = agents[other.agent];

        auto S1 =(vec2f32) graph.vertices[other.state.vertex].pos;
        auto S2 = (vec2f32)graph.vertices[target.state.vertex].pos;
        auto d = target_agent.radius + other_agent.radius + 2.0f* RADIUS_EPSILON;
        auto dt = target.action.start_time - other.action.start_time;

        auto v1 = transitions_vec[other.action.transition_id] * other_agent.movement_speed;
        auto v2 = transitions_vec[target.action.transition_id] * target_agent.movement_speed;

        auto dv = v1 - v2;
        auto a1 = dot(dv, dv);
        auto Qbis = S1 - S2 + v2 * dt;

        auto k1 = 2.0f * dot(v2,dv);
        auto k2 = 2.0f * dot(Qbis, dv);
        auto k3 = dot(v2, v2);
        auto k4 = 2.0f * dot(Qbis, v2);
        auto k5 = dot(Qbis, Qbis) - d * d;

        auto a2 = k1 * k1 - 4.0f * a1 * k3;
        if (std::abs(a2) < 0.001f) {
            if (coll.collision_interval.end <= coll.overlap_interval.end) {
                target.interval.end = coll.overlap_interval.end;
                return;
            }
            // The problem collapses when two agents go straight at each other
            // Solve the "Constrained agent moving/other agent waiting at the end of its movement" problem
            Collision coll2;
            coll2.a1 = target;
            coll2.a2 = other;
            coll2.a2.state.vertex = coll2.a2.action.target_vertex;
            coll2.a2.action.type = ActionType::WAIT;
            coll2.a2.action.start_time = 0.0;
            coll2.a2.end_time = std::numeric_limits<float>::infinity();
            if (!check_collisions(coll2)) {
                throw_error("Expected collision");
            }
            if (target.end_time > other.end_time) {
                auto delay = other.end_time - coll2.collision_interval.start;
                if (delay < -0.00001f) throw_error("Delay negative");
                auto interval_end = target.action.start_time + delay;
                target.interval.end = std::min(interval_end, coll.overlap_interval.end) + CONSTRAINT_EPSILON; // Constrained intervals can only be between the 2 actions
            }
            else {
                auto travel_time = target.end_time - target.action.start_time;
                target.interval.end = other.end_time - travel_time +CONSTRAINT_EPSILON;
            }

            return;
        }
        auto b2 = 2.0f*k1*k2 - 4.0f * a1 * k4;
        auto c2 = k2 * k2 - 4.0f * a1 * k5;

        auto rho2 = b2 * b2 - 4.0f * a2 * c2;
        if (rho2 < 0.00001f) {
            throw_error("collision resolve rho error");
        }
        auto sqrt_rho = std::sqrt(rho2);
        auto w1 = (-b2 + sqrt_rho) / (2.0f * a2);
        auto w2 = (-b2 - sqrt_rho) / (2.0f * a2);
        if (w1 > w2) std::swap(w1, w2);

        if (w1 > 0) throw_error("Expected one negative waiting time");
        if (w2 < 0) throw_error("Expected one positive waiting time");

        auto interval_end = target.interval.start + w2;
        target.interval.end = std::min(interval_end, coll.overlap_interval.end) + CONSTRAINT_EPSILON; // Constrained intervals can only be between the 2 actions
    }


    f32 Grid::get_clearance(vec2i32 pos, vec2i32 target_pos)
    {
        auto v = target_pos - pos;
        auto normal = normalize(vec2f32((float)-v.y, (float)v.x));
        auto min_x = std::min(pos.x, target_pos.x);
        auto min_y = std::min(pos.y, target_pos.y);
        auto max_x = std::max(pos.x, target_pos.x);
        auto max_y = std::max(pos.y, target_pos.y);
        f32 clearance = std::numeric_limits<f32>::infinity();
        for (i32 y = min_y; y <= max_y; ++y) {
            for (i32 x = min_x; x <= max_x; ++x) {
                auto cell_pos = vec2i32{ x,y };
                if (!is_blocked(cell_pos)) continue;
                bool has_plus = false;
                bool has_minus = false;
                vec2f32 pos_to_cell = (vec2f32)(cell_pos - pos);
                for (auto& c : cell_corners) {
                    vec2f32 pos_to_corner = pos_to_cell + c;
                    auto proj = dot(normal, pos_to_corner);
                    if (proj < 0) {
                        if (has_plus) return 0.0f;
                        has_minus = true;
                        proj = -proj;
                    }
                    else {
                        if (has_minus) return 0.0f;
                        has_plus = true;
                    }
                    if (proj < clearance) {
                        clearance = proj;
                    }
                }
            }
        }
        return clearance;
    }
    void Node::compute_f_value()
    {
        f_value = 0.0;
        for (auto& t : solution) {
            f_value += t.cost;
        }
    }

    std::string Action::to_string(const Graph& g) const {
        std::string action;
        switch (type) {
        case ActionType::MOVE: {
            auto& v = g.vertices[target_vertex];
            action = "MOVE to [" + std::to_string(v.pos.x) + ", " + std::to_string(v.pos.y) + "]";
        } break;
        case ActionType::ROTATE:
            action = "ROTATE to head " + std::to_string(transition_id) + "/" + std::to_string(transitions_heading[transition_id]) + "deg";
            break;
        case ActionType::WAIT:
            action = "WAIT";
            break;
        }
        return action + " at " + std::to_string(start_time)+"s";
    }

    void Collision::print(const Graph &g)
    {
        std::cout << "Collision between agent" << std::endl;
        std::cout << "    A" << (a1.agent+1) << " " << a1.state.to_string(g) << "performing " << a1.action.to_string(g);
        std::cout << " [" << a1.action.start_time << "s to " << a1.end_time << "s]" << std::endl;
        std::cout << "and agent" << std::endl;
        std::cout << "    A" << (a2.agent + 1) << " " << a2.state.to_string(g) << "performing " << a2.action.to_string(g);
        std::cout << " [" << a2.action.start_time << "s to " << a2.end_time << "s]" << std::endl;
    }
    std::string State::to_string(const Graph &g) const
    {
        return "[pos=" + g.vertices[vertex].pos.to_string() + ", h=" + std::to_string(transition_id) + "/" + std::to_string(transitions_heading[transition_id]) + "deg]";
    }
}