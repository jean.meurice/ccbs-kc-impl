#include <iostream>
#include "xml.h"
#include "cbs.h"






int main(int argc, char**argv) {
    try {
        if (argc < 2) {
            std::cout << "Usage:" << std::endl;
            std::cout << "   ccbs [options] <config>" << std::endl;
            std::cout << "Options:" << std::endl;
            std::cout << "   -es     Export the Solution of all nodes." << std::endl;
            std::cout << "   -nt     No Timeout." << std::endl;
            std::cout << "   -is     Export the intermediary solutions of the final solution." << std::endl;
            return -1;
        }
        auto data = std::make_unique<cbs::ProgramData>(fs::path(argv[argc - 1]));
        int i = 1;
        for (; i < argc-1; ++i) {
            if (std::string(argv[i]) == "-es") {
                data->params.export_node_solutions = true;
            }
            else if (std::string(argv[i]) == "-nt") {
                data->params.no_timeout = true;
            }
            else if (std::string(argv[i]) == "-is") {
                data->params.export_intermediary_solutions = true;
            }
            else {
                throw_error("Unknown flag: " + std::string(argv[i]));
            }
        }
        //svg::test_svg_export({"gen_test.svg"});
        data->entry_point();
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}