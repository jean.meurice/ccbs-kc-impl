#pragma once
#include <cstdint>
#include <exception>
#include <string>
#include <initializer_list>
#include <chrono>
#include <iostream>
#include <filesystem>
namespace fs = std::filesystem;

class SomeException : public std::exception {
    std::string description;
public:
    SomeException(const std::string& description) : description(description) {}
    SomeException(const std::string& description, const char* file, int line) : description(
        "Exception at " + std::string(file) + "[line: " + std::to_string(line) + "]: " + description
    ) {}
    virtual const char* what() const throw()
    {
        return description.c_str();
    }
};

#define throw_error(MESSAGE) throw SomeException(MESSAGE, __FILE__, __LINE__)


struct Timer {
    using clock = std::chrono::high_resolution_clock;
    using dur_sec = std::chrono::duration<double>;
    using dur_ms = std::chrono::duration<double, std::ratio<1, 1000>>;
    using dur_us = std::chrono::duration<double, std::ratio<1, 1000000>>;

    clock::time_point start_t;
    clock::time_point end_t;

    void start() {
        start_t = clock::now();
    }
    void end() {
        end_t = clock::now();
    }
    double duration() const {
        return std::chrono::duration_cast<dur_sec>(end_t - start_t).count();
    }
    double duration_ms() const {
        return std::chrono::duration_cast<dur_ms>(end_t - start_t).count();
    }
    double duration_us() const {
        return std::chrono::duration_cast<dur_us>(end_t - start_t).count();
    }
};


using i8 = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;

using u8 = uint8_t;
using u16 = uint16_t;
//using i32 = uint32_t;
using u64 = uint64_t;

using usize = uintptr_t;

using f32 = float;
using f64 = double;



static constexpr float PIf = 3.14159265358979323846f;
static constexpr double PId = 3.14159265358979323846;

static constexpr float DEG_TO_RADf = 2.0f / 360.0f * PIf;
static constexpr double DEG_TO_RADd = 2.0 / 360.0 * PId;
static constexpr float RAD_TO_DEGf = 360.0f / (2.0f * PIf);
static constexpr double RAD_TO_DEGd = 360.0 / (2.0 * PId);

/*
    Vector of length 2 and type <T>.
    Members are accessed through x and y; min and max; data or operator[].
*/
template<typename T>
struct vec2 {
    union {
        struct {
            T x;
            T y;
        };
        T data[2];
        struct {
            T min;
            T max;
        };
    };
    constexpr vec2<T>( T x, T y ) : x( x ), y( y ) {}
    constexpr vec2<T>( T *data ) : x( data[0] ), y( data[1] ) {}
    constexpr vec2<T>() {}
    constexpr vec2<T>( T val ) : x( val ), y( val ) {}
    /*vec2<T>(std::initializer_list<T> l) {
        if (l.size() != 2) {
            throw_error("Called initializer list on vec2 of invalid size");
        }
        x = l.begin()[0];
        y = l.begin()[1];
    }*/
    
    
    constexpr vec2<T> operator+( const vec2<T> &add ) const {
        return vec2<T>( x + add.x, y + add.y );
    }
    constexpr vec2<T> operator-( const vec2<T> &sub ) const {
        return vec2<T>( x - sub.x, y - sub.y );
    }
    
    //Component-wise multiplication
    constexpr vec2<T> operator*( const vec2<T> &mult ) const {
		return vec2<T>( x * mult.x, y * mult.y );
    }
    
    vec2<T> &operator += ( const vec2<T> &add ) {
        x += add.x;
        y += add.y;
        return *this;
    }
    vec2<T> &operator -= ( const vec2<T> &sub ) {
        x -= sub.x;
        y -= sub.y;
        return *this;
    }
    
    constexpr vec2<T> operator-() const {
        return vec2<T>( -x, -y );
    }
    
#define vec_scalar_op(OP) constexpr vec2<T> operator OP ( const T& val ) const { return vec2<T>(x OP val, y OP val); }
    vec_scalar_op( * )
    vec_scalar_op( / )
    vec_scalar_op( + )
    vec_scalar_op( - )
    vec_scalar_op( % )
#undef vec_scalar_op
    
#define vec_scalar_op(OP) vec2<T> &operator OP ( const T& val ) { x OP val; y OP val; return *this; }
    vec_scalar_op( *= )
    vec_scalar_op( /= )
    vec_scalar_op( += )
    vec_scalar_op( -= )
    vec_scalar_op( %= )
#undef vec_scalar_op
    
    //Cartesian length of the vector
    T operator!() const {
        return ( T ) std::sqrt( x * x + y * y );
    }
    
    //Cartesian length of the vector, also use !vec
    T length() const {
        return !*this;
    }
    
    //Returns normalized vector
    vec2 operator~() const {
        auto l = length();
        return l == 0 ? *this : *this / l;
    }
    
    //Returns a the normalized vector, also use ~vec.
    vec2 normalized() const {
        return ~*this;
    }
    
    inline T &operator[]( i32 i ) {
        //throw_assert( i < 2, "Vector data access param too big" );
        return data[i];
    }
    inline const T &operator[]( i32 i ) const {
        //throw_assert( i < 2, "Vector data access param too big" );
        return data[i];
    }
    
    template<typename A>
    explicit operator vec2<A>() const {
        return vec2<A>( ( A )x, ( A )y );
    }

    std::string to_string() const {
        return "[" + std::to_string(x) + ", " + std::to_string(y) + "]";
    }
};



/*
Vector of length 3 and type <T>.
Members are accessed through x, y and z; r g and b; data or operator[].
*/
template<typename T>
struct vec3 {
    union {
        struct {
            T x;
            T y;
            T z;
        };
        T data[3];
        struct {
            T r;
            T g;
            T b;
        };
    };
    vec3<T>( T x, T y, T z ) : x( x ), y( y ), z( z ) {}
    vec3<T>() {}
    vec3<T>( T val ) : x( val ), y( val ), z( val ) {}
    vec3<T>( T *data ) : x( data[0] ), y( data[1] ), z( data[2] ) {}
    
    vec3( const vec2<T> &v1, const T &val ) : x( v1.x ), y( v1.y ), z( val ) {}
    vec3( const T &val, const vec2<T> &v1 ) : x( val ), y( v1.x ), z( v1.y ) {}
    
    vec3<T> operator+( const vec3<T> &add ) const {
        return vec3<T>( x + add.x, y + add.y, z + add.z );
    }
    vec3<T> operator-( const vec3<T> &sub ) const {
        return vec3<T>( x - sub.x, y - sub.y, z - sub.z );
    }
    
    //Component-wise multiplication
    inline vec3<T> operator*( const vec3<T> &mult ) const {
		return vec3<T>( x * mult.x, y * mult.y, z * mult.z );
    }
    
    vec3<T> &operator += ( const vec3<T> &add ) {
        x += add.x;
        y += add.y;
        z += add.z;
        return *this;
    }
    vec3<T> &operator -= ( const vec3<T> &sub ) {
        x -= sub.x;
        y -= sub.y;
        z -= sub.z;
        return *this;
    }
    
    vec3<T> operator-() const {
        return vec3<T>( -x, -y, -z );
    }
    
#define vec_scalar_op(OP) vec3<T> operator OP ( const T& val ) const { return vec3<T>(x OP val, y OP val, z OP val); }
    vec_scalar_op( * )
    vec_scalar_op( / )
    vec_scalar_op( + )
    vec_scalar_op( - )
    vec_scalar_op( % )
#undef vec_scalar_op
    
#define vec_scalar_op(OP) vec3<T> &operator OP ( const T& val ) { x OP val; y OP val; z OP val; return *this; }
    vec_scalar_op( *= )
    vec_scalar_op( /= )
    vec_scalar_op( += )
    vec_scalar_op( -= )
    vec_scalar_op( %= )
#undef vec_scalar_op
    
    //Cartesian length of the vector
    T operator!() const {
        return ( T )sqrt( x * x + y * y + z * z );
    }
    
    //Cartesian length of the vector, also use !vec
    T length() const {
        return !*this;
    }
    inline T &operator[]( i32 i ) {
        //throw_assert( i < 3, "Vector data access param too big" );
        return data[i];
    }
    inline const T &operator[]( i32 i ) const {
        //throw_assert( i < 3, "Vector data access param too big" );
        return data[i];
    }
    
    //Returns normalized vector
    vec3 operator~() const {
        auto l = length();
        return l == 0 ? *this : *this / l;
    }
    
    //Returns a the normalized vector, also use ~vec.
    vec3 normalized() const {
        return ~*this;
    }
    
    /*
        Returns the cross product of two <vec3>, using right hand rule.
    */
    vec3<T> operator^(const vec3<T> &other ) const {
        return vec3<T>( y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x );
    }
    
    //Cross product: also use v1 ^ v2
    template<typename T>
    vec3<T> cross( const vec3<T> &v2 ) const {
        return *this ^ v2;
    }
    
    template<typename A>
    explicit operator vec3<A>() const {
        return vec3<A>( ( A )x, ( A )y, ( A )z );
    }
};

/*
    Vector of length 4 and type <T>.
    Members are accessed through x, y, z and u; r, g, b and a; data or operator[].
    Use length() to get the cartesian length of the vector.
    The Vertex addition and substration, as well as scalar multiplication and division are defined.
*/
template<typename T>
struct vec4 {
    union {
        struct {
            T x;
            T y;
            T z;
            T u;
        };
        T data[4];
        struct {
            T r;
            T g;
            T b;
            T a;
        };
    };
    
    inline vec4<T>( T x, T y, T z, T u ) : x( x ), y( y ), z( z ), u( u ) {}
    inline vec4<T>( T *data ) : x( data[0] ), y( data[1] ), z( data[2] ), u( data[3] ) {}
    inline vec4<T>() {}
    inline vec4<T>( T val ) : x( val ), y( val ), z( val ), u( val ) {}
    
    
    inline vec4<T> operator+( const vec4<T> &add ) const {
        return vec4<T>( x + add.x, y + add.y, z + add.z, u + add.u );
    }
    inline vec4<T> operator-( const vec4<T> &sub ) const {
        return vec4<T>( x - sub.x, y - sub.y, z - sub.z, u - sub.u );
    }
    
    //Component-wise multiplication
    inline vec4<T> operator*( const vec4<T> &mult ) const {
		return vec4<T>( x * mult.x, y * mult.y, z * mult.z, u * mult.u );
    }
    
    inline vec4<T> &operator += ( const vec4<T> &add ) {
        x += add.x;
        y += add.y;
        z += add.z;
        u += add.u;
        return *this;
    }
    inline vec4<T> &operator -= ( const vec4<T> &sub ) {
        x -= sub.x;
        y -= sub.y;
        z -= sub.z;
        u -= sub.u;
        return *this;
    }
    
    inline vec4<T> operator-() const {
        return vec4<T>( -x, -y, -z, -u );
    }
    
#define vec_scalar_op(OP) inline vec4<T> operator OP ( const T& val ) const { \
        return vec4<T>(x OP val, y OP val, z OP val, u OP val); \
    }
    vec_scalar_op( * )
    vec_scalar_op( / )
    vec_scalar_op( + )
    vec_scalar_op( - )
    vec_scalar_op( % )
#undef vec_scalar_op
    
#define vec_scalar_op(OP) inline vec4<T> &operator OP ( const T& val ) { x OP val; y OP val; z OP val; a OP val; return *this; }
    vec_scalar_op( *= )
    vec_scalar_op( /= )
    vec_scalar_op( += )
    vec_scalar_op( -= )
    vec_scalar_op( %= )
#undef vec_scalar_op
    
    //Returns the length of the vector
    inline T operator!() const {
        return ( T )sqrt( x * x + y * y + z * z + u * u );
    }
    
    //Cartesian length of the vector, also use !vec.
    inline T length() const {
        return !*this;
    }
    
    //Returns normalized vector
    inline vec4 operator~() const {
        auto l = length();
        return l == 0 ? *this : *this / l;
    }
    
    //Returns a the normalized vector, also use ~vec.
    inline vec4 normalized() const {
        return ~*this;
    }

	inline vec3<T> xyz() const {
        return vec3<T>(x,y,z);
    }
    
    inline T &operator[]( i32 i ) {
        //throw_assert( i < 4, "Vector data access param too big" );
        return data[i];
    }
    inline const T &operator[]( i32 i ) const {
        //throw_assert( i < 4, "Vector data access param too big" );
        return data[i];
    }
    
    template<typename A>
    explicit operator vec4<A>() const {
        return vec4<A>( ( A )x, ( A )y, ( A )z, ( A ) a );
    }
};


/*
    Type shortcuts for common vector types.
*/
using  vec2i8 = vec2<i8>;
using  vec3i8 = vec3<i8>;
using  vec4i8 = vec4<i8>;

using  vec2u8 = vec2<u8>;
using  vec3u8 = vec3<u8>;
using  vec4u8 = vec4<u8>;

using  vec2i16 = vec2<i16>;
using  vec3i16 = vec3<i16>;
using  vec4i16 = vec4<i16>;

using  vec2u16 = vec2<u16>;
using  vec3u16 = vec3<u16>;
using  vec4u16 = vec4<u16>;

using  vec2i32 = vec2<i32>;
using  vec3i32 = vec3<i32>;
using  vec4i32 = vec4<i32>;

//using  vec2i32 = vec2<i32>;
//using  vec3i32 = vec3<i32>;
//using  vec4i32 = vec4<i32>;

using  vec2i64 = vec2<i64>;
using  vec3i64 = vec3<i64>;
using  vec4i64 = vec4<i64>;

using  vec2u64 = vec2<u64>;
using  vec3u64 = vec3<u64>;
using  vec4u64 = vec4<u64>;

using  vec2f32 = vec2<f32>;
using  vec3f32 = vec3<f32>;
using  vec4f32 = vec4<f32>;

using  vec2f64 = vec2<f64>;
using  vec3f64 = vec3<f64>;
using  vec4f64 = vec4<f64>;



/*
Returns the normalized <v> vector.
Returns the zero vector if given the zero vector.
*/
template<template<typename> typename U>
inline U<double> normalize(const U<double>& v) {
    auto l = v.length();

    if (l < 0.00000001 && l > -0.00000001)
        return v;

    return v / l;
}

/*
Returns the normalized <v> vector.
Returns the zero vector if given the zero vector.
*/
template<template<typename> typename U>
inline U<float> normalize(const U<float>& v) {
    auto l = v.length();

    if (l < 0.00000001 && l > -0.00000001)
        return v;

    return v / l;
}

template<typename T>
inline T dot(const vec2<T>& v1, const vec2<T>& v2) {
    return v1.x * v2.x + v1.y * v2.y;
}


template<typename T>
std::ostream& operator<<(std::ostream& out, const vec2<T>& vec) {
    out << "[" << vec.x << ", " << vec.y << "]";
    return out;
}