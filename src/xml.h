#pragma once
#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include "utils.h"

namespace xml {
    constexpr auto INDENT = "  ";
    struct Attribute {
        std::string name;
        std::string value; // Escaped when exported
        Attribute(std::string name, std::string value) : name(name), value(value) {}
    };

    struct XMLElem {

        static XMLElem *new_text(std::string &content) {
            return new XMLElem(true, content);
        }
        static XMLElem *new_tag(std::string &name) {
            return new XMLElem(false, name);
        }
        static XMLElem *new_text(const char *content) {
            return new XMLElem(true, content);
        }
        static XMLElem *new_tag(const char *name) {
            return new XMLElem(false, name);
        }

        void add(XMLElem *elem) {
            children.emplace_back(elem);
        }

        void add_attribute(std::string name, std::string value) {
            attributes.emplace_back(name, value);
        }

        bool is_text() const {
            return m_is_text;
        }

        void print(std::ofstream &file, const std::string &indent, bool format) const ;
        XMLElem* clone();

    private:
        bool m_is_text;
        std::string name_or_text;
        std::vector<std::unique_ptr<XMLElem>> children;
        std::vector<Attribute> attributes;

        XMLElem(bool is_text, std::string &name_or_text) : m_is_text(is_text), name_or_text(name_or_text) {}
        XMLElem(bool is_text, const char *name_or_text) : m_is_text(is_text), name_or_text(name_or_text) {}
    };

    std::string escape(const std::string &text);

    void export_xml(const XMLElem &xml, fs::path &target, bool format);
}

namespace svg {

    struct PathCreator {

        xml::XMLElem* get_tag();
        void move_to(vec2f32 pos) {
            move_to(pos.x, pos.y);
        }
        void line_to(vec2f32 pos) {
            line_to(pos.x, pos.y);
        }
        void move_to(f32 x, f32 y);
        void line_to(f32 x, f32 y);
        void close() {
            closed = true;
        }

    private:
        bool closed = false;
        enum CommandType {
            MOVE,
            LINE_TO
        };
        struct Command {
            CommandType type;
            vec2f32 point;
        };
        std::vector<Command> commands;
    };

    std::unique_ptr<xml::XMLElem> new_svg();
    void set_viewport(xml::XMLElem &svg_tag, vec2i32 dimensions);

    xml::XMLElem* new_circle(float radius, vec2f32 position);
    xml::XMLElem* new_text(vec2f32 position, std::string content);
    xml::XMLElem* new_rect(vec2f32 position, vec2f32 size);

    void test_svg_export(fs::path target);
}