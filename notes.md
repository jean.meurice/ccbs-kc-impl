# Notes

## Bugs

- case5-bis => timeout
- case6 => timeout

## TODOs

- [ ] Export
  - [X] Draw trajectories
  - [X] Map coordinates (on the side)
  - [X] Export intermediary solutions (on valid solution) => flag?
  - [X] JSON
    - [X] Nodes explored
    - [X] Constraint list
    - [X] Node list
  - [ ] Stop animation on collision ?
  - [ ] Agent movement
    - [ ] Timeline

Probably no time:

- [ ] Reuse safe-intervals arrays
- [ ] Heuristic with rotations
- [ ] Collision Lookup Grid
- [ ] Edge clearance & vertex clearance if radii are bigger than 0.5

## PriorityQueue

- https://stackoverflow.com/questions/5895792/why-is-using-a-stdmultiset-as-a-priority-queue-faster-than-using-a-stdpriori
- Using 'priority_queue' https://en.cppreference.com/w/cpp/container/priority_queue
  - top(), pop(), push(), empty()
  - give template parameter to comparison object (with operator() ??)
- Using 'multiset': https://www.cplusplus.com/reference/set/multiset/
  - begin() gives iterator to first element: constant time
  - erase(it) removes the element: amortized constant
  - insert(elem) logarithmic
  - give template parameter to comparison object (with operator() ??)

## SVG export

- [Sablier](http://www.unicode-symbol.com/u/231B.html)
- Couleur par agent ? => + "share" les edges pour les traits ? + cercle de la largeur des traits au niveau des vertices
