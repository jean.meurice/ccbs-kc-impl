
## Notes

- https://css-tricks.com/guide-svg-animations-smil/
https://developer.mozilla.org/en-US/docs/Web/SVG/Element/animate
https://developer.mozilla.org/en-US/docs/Web/SVG/Element/g
https://stackoverflow.com/questions/41872057/animating-an-svg-group 
- Animate transform: https://developer.mozilla.org/en-US/docs/Web/SVG/Element/animateTransform

=> One animation tag per agent, use 'values' list & 'keyTimes' list

## Works

```xml
<svg width="500px" height="200px" viewBox="0 0 500 200" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

    <g transform="translate(40,100)">
        <circle id="my-circle" r="30" cx="50" cy="50" fill="orange" />
        <text x="50" y="50" font-size="20" text-anchor="middle" fill="black">Test</text>
    </g>
    <animate xlink:href="#my-circle" attributeName="cx" values="50;300;50" dur="1s" begin="0s" fill="freeze" />
    <animate xlink:href="#my-circle" attributeName="cx" from="50" to="450" dur="1s" begin="2s" fill="freeze" />
    <animate xlink:href="#my-circle" attributeName="cx" to="200" dur="1s" begin="4s" fill="freeze" />

</svg>
```