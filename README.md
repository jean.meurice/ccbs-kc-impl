This is a partial re-implementation of [MAPF with Kinematic Constraints via CBS (A. Andreychuk, 2020)](https://www.semanticscholar.org/paper/Multi-agent-Path-Finding-with-Kinematic-Constraints-Andreychuk/bad0948ffb73da993c1942305817d39d0f95a108)

Build with CMake (as in the `build.bat` script).

Run `ccbs` in the bin folder with a scenario name as argument.

It will perform the search and export an animated svg.
